## Terminal

Uso de automação de processos através de comandos.

Acesso ao terminal 
1. ctrl + alt + t
2. através da barra de pesquisa

## comandos no terminal
* Rodar como adminstrador através do sudo
' $ sudo su ' 
' 12345678'  


* comando pwd : O pwd é um comando que imprime o nome do diretório local em uma interface de linha de comando.
' $ pwd ' 

* comando ls: list files and directories
 - variações  
    a. '$ ls - l ' 
    b. '$ ls - lf' 


* comando mkdir : criar um novo diretório
    - 

* comando man : acessar ao manual do comando
    - ' $ man pwd' 

* comando history: mostrar o histórico de comandos digitados


## atalhos no terminal

* ctrl + c : cancela o comando atual em funcionamento
* ctrl + z : pausa o comando atual, em primeiro ou segundo plano
* ctrl + D : faz o logout da sessao atual
* ctrl + W : apaga a palavra na linha atual
* ctrl + u : apaga a linha inteira
* ctrl + r : busca um comando recente